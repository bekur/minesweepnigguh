package com.example.minesweeper
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_game.*
import kotlinx.android.synthetic.main.activity_sign_up.*
class SignUpActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        auth = FirebaseAuth.getInstance()
        back()
        checkFields()
    }

    private fun back() {
        backButton_signUp.setOnClickListener {
            val intent = Intent(this, LogInActivity::class.java)
            startActivity(intent)
        }
    }

    private fun checkFields() {
        SignUpButton_signUp.setOnClickListener {
            if (emailField_signUp.text.isEmpty() && passwordField_signUp.text.isNotEmpty() && passwordField2_signUp.text.isNotEmpty()) {
                Toast.makeText(this, "გთხოვთ, შეიყვანოთ თქვენი იმეილი", Toast.LENGTH_SHORT).show()
            } else if (emailField_signUp.text.isNotEmpty() && passwordField_signUp.text.isEmpty() && passwordField2_signUp.text.isEmpty()) {
                Toast.makeText(this, "გთხოვთ, შეიყვანოთ თქვენი პაროლი", Toast.LENGTH_SHORT).show()
            } else if (emailField_signUp.text.isNotEmpty() && passwordField_signUp.text.isNotEmpty() && passwordField2_signUp.text.isEmpty()) {
                Toast.makeText(this, "გთხოვთ, ხელახლა შეიყვანოთ თქვენი პაროლი", Toast.LENGTH_SHORT).show()
            } else if (emailField_signUp.text.isEmpty() && passwordField_signUp.text.isEmpty() or passwordField2_signUp.text.isEmpty()) {
                Toast.makeText(this, "გთხოვთ, შეავსოთ ყველა ველი", Toast.LENGTH_SHORT)
                    .show()
            } else if (emailField_signUp.text.isNotEmpty() && passwordField_signUp.text.toString() != passwordField2_signUp.text.toString()) {
                Toast.makeText(this, "პაროლები ერთმანეთს არ ემთხვევა", Toast.LENGTH_SHORT).show()
            } else {
                fire()
            }
        }
    }
    private fun fire() {
        auth.createUserWithEmailAndPassword(emailField_signUp.text.toString(), passwordField_signUp.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Toast.makeText(
                        baseContext, "რეგისტრაცია წარმატებით დასრულდა",
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    Toast.makeText(
                        baseContext, "ავტორიზაცია ვერ მოხერხდა",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
    }
}
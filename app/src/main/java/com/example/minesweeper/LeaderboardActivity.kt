package com.example.minesweeper

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_leaderboard.*

class LeaderboardActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leaderboard)
        downloadRecords()
        back()
    }

//    fun main() {
//        val scores = arrayOf(Score("Alex", 5), Score("Bob", 2), Score("Harry", 1))
//
//        println("Original array : ")
//        scores.forEach { println(it) }
//
//        scores.sortBy { student -> student.points}
//
//        println("Final array : ")
//        scores.forEach { println(it) }
//    }

    data class Score(val User: String, val points: Int)


    private fun back() {
        backButton_leaderboard.setOnClickListener {
            val intent = Intent(this, GameActivity::class.java)
            startActivity(intent)
        }
    }
    private fun downloadRecords(){
        val database = FirebaseDatabase.getInstance()
        val myRef = database.getReference("LeaderboardModel")
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                val pointsValue = dataSnapshot.getValue(LeaderboardModel.OnlineRecordPoints::class.java)
                val usersValue = dataSnapshot.getValue(LeaderboardModel.OnlineRecordUsers::class.java)
                onlRecPlace_1_points.text = pointsValue!!.onlRecPlace_1_points.toString()
                onlRecPlace_2_points.text = pointsValue.onlRecPlace_2_points.toString()
                onlRecPlace_3_points.text = pointsValue.onlRecPlace_3_points.toString()
                onlRecPlace_4_points.text = pointsValue.onlRecPlace_4_points.toString()
                onlRecPlace_5_points.text = pointsValue.onlRecPlace_5_points.toString()
                onlRecPlace_6_points.text = pointsValue.onlRecPlace_6_points.toString()
                onlRecPlace_7_points.text = pointsValue.onlRecPlace_7_points.toString()
                onlRecPlace_8_points.text = pointsValue.onlRecPlace_8_points.toString()

                onlRecPlace_1_user.text = usersValue!!.onlRecPlace_1_user
                onlRecPlace_2_user.text = usersValue.onlRecPlace_2_user
                onlRecPlace_3_user.text = usersValue.onlRecPlace_3_user
                onlRecPlace_4_user.text = usersValue.onlRecPlace_4_user
                onlRecPlace_5_user.text = usersValue.onlRecPlace_5_user
                onlRecPlace_6_user.text = usersValue.onlRecPlace_6_user
                onlRecPlace_7_user.text = usersValue.onlRecPlace_7_user
                onlRecPlace_8_user.text = usersValue.onlRecPlace_8_user
            }
            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.w("onDataChange", "Failed to read value.", error.toException())
            }
        })
    }
}

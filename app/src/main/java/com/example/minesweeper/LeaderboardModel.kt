package com.example.minesweeper

class LeaderboardModel {
    object OnlineRecordPoints {
        var onlRecPlace_1_points : Int = 0
        var onlRecPlace_2_points : Int = 0
        var onlRecPlace_3_points : Int = 0
        var onlRecPlace_4_points : Int = 0
        var onlRecPlace_5_points : Int = 0
        var onlRecPlace_6_points : Int = 0
        var onlRecPlace_7_points : Int = 0
        var onlRecPlace_8_points : Int = 0
    }
    object OnlineRecordUsers {
        var onlRecPlace_1_user : String = ""
        var onlRecPlace_2_user : String = ""
        var onlRecPlace_3_user : String = ""
        var onlRecPlace_4_user : String = ""
        var onlRecPlace_5_user : String = ""
        var onlRecPlace_6_user : String = ""
        var onlRecPlace_7_user : String = ""
        var onlRecPlace_8_user : String = ""
    }
}

//"LeaderboardModel" : {
//    "OnlineRecord_Points" : {
//        "onlRecPlace_1_points" : 1124,
//        "onlRecPlace_2_points" : 1065,
//        "onlRecPlace_3_points" : 957,
//        "onlRecPlace_4_points" : 813,
//        "onlRecPlace_5_points" : 651,
//        "onlRecPlace_6_points" : 517,
//        "onlRecPlace_7_points" : 336,
//        "onlRecPlace_8_points" : 193
//    },
//    "OnlineRecord_Users" : {
//        "onlRecPlace_1_user" : "jemala.1@btu.edu.ge",
//        "onlRecPlace_2_user" : "mia.khalifa.1@btu.edu.ge",
//        "onlRecPlace_3_user" : "murtaza.1@btu.edu.ge",
//        "onlRecPlace_4_user" : "badri.shubladze.1@btu.edu.ge",
//        "onlRecPlace_5_user" : "janiko.1@btu.edu.ge",
//        "onlRecPlace_6_user" : "kuzanov.1@btu.edu.ge",
//        "onlRecPlace_7_user" : "ricardo.1@btu.edu.ge",
//        "onlRecPlace_8_user" : "kokhora.1@btu.edu.ge"
//    }
//}

package com.example.minesweeper

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.core.graphics.drawable.toDrawable
import kotlinx.android.synthetic.main.activity_game.*
import java.lang.reflect.Array
import java.util.*
import kotlin.random.Random
import kotlin.String as String1


class GameActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        setDefaultImagesToButtons()
        guidePage()
        accountPage()
        leaderboardPage()
        countTime()

        startGame()
        openCell(b00)
        openCell(b01)
        openCell(b02)
        openCell(b03)
        openCell(b04)
        openCell(b05)
        openCell(b06)
        openCell(b07)
        openCell(b08)
        openCell(b10)
        openCell(b11)
        openCell(b12)
        openCell(b13)
        openCell(b14)
        openCell(b15)
        openCell(b16)
        openCell(b17)
        openCell(b18)
        openCell(b20)
        openCell(b21)
        openCell(b22)
        openCell(b23)
        openCell(b24)
        openCell(b25)
        openCell(b26)
        openCell(b27)
        openCell(b28)
        openCell(b30)
        openCell(b31)
        openCell(b32)
        openCell(b33)
        openCell(b34)
        openCell(b35)
        openCell(b36)
        openCell(b37)
        openCell(b38)
        openCell(b40)
        openCell(b41)
        openCell(b42)
        openCell(b43)
        openCell(b44)
        openCell(b45)
        openCell(b46)
        openCell(b47)
        openCell(b48)
        openCell(b50)
        openCell(b51)
        openCell(b52)
        openCell(b53)
        openCell(b54)
        openCell(b55)
        openCell(b56)
        openCell(b57)
        openCell(b58)
        openCell(b60)
        openCell(b61)
        openCell(b62)
        openCell(b63)
        openCell(b64)
        openCell(b65)
        openCell(b66)
        openCell(b67)
        openCell(b68)
        openCell(b70)
        openCell(b71)
        openCell(b72)
        openCell(b73)
        openCell(b74)
        openCell(b75)
        openCell(b76)
        openCell(b77)
        openCell(b78)
        openCell(b80)
        openCell(b81)
        openCell(b82)
        openCell(b83)
        openCell(b84)
        openCell(b85)
        openCell(b86)
        openCell(b87)
        openCell(b88)

        setFlag(b00)
        setFlag(b01)
        setFlag(b02)
        setFlag(b03)
        setFlag(b04)
        setFlag(b05)
        setFlag(b06)
        setFlag(b07)
        setFlag(b08)
        setFlag(b10)
        setFlag(b11)
        setFlag(b12)
        setFlag(b13)
        setFlag(b14)
        setFlag(b15)
        setFlag(b16)
        setFlag(b17)
        setFlag(b18)
        setFlag(b20)
        setFlag(b21)
        setFlag(b22)
        setFlag(b23)
        setFlag(b24)
        setFlag(b25)
        setFlag(b26)
        setFlag(b27)
        setFlag(b28)
        setFlag(b30)
        setFlag(b31)
        setFlag(b32)
        setFlag(b33)
        setFlag(b34)
        setFlag(b35)
        setFlag(b36)
        setFlag(b37)
        setFlag(b38)
        setFlag(b40)
        setFlag(b41)
        setFlag(b42)
        setFlag(b43)
        setFlag(b44)
        setFlag(b45)
        setFlag(b46)
        setFlag(b47)
        setFlag(b48)
        setFlag(b50)
        setFlag(b51)
        setFlag(b52)
        setFlag(b53)
        setFlag(b54)
        setFlag(b55)
        setFlag(b56)
        setFlag(b57)
        setFlag(b58)
        setFlag(b60)
        setFlag(b61)
        setFlag(b62)
        setFlag(b63)
        setFlag(b64)
        setFlag(b65)
        setFlag(b66)
        setFlag(b67)
        setFlag(b68)
        setFlag(b70)
        setFlag(b71)
        setFlag(b72)
        setFlag(b73)
        setFlag(b74)
        setFlag(b75)
        setFlag(b76)
        setFlag(b77)
        setFlag(b78)
        setFlag(b80)
        setFlag(b81)
        setFlag(b82)
        setFlag(b83)
        setFlag(b84)
        setFlag(b85)
        setFlag(b86)
        setFlag(b87)
        setFlag(b88)




        newGameButton.setOnClickListener {
            startGame()
        }


    }

    private fun startGame() {
        currentTimeView.text = "000"
        currentPointsView.text = "0"
        currentTime = 0

        newGameButton.setBackgroundResource(R.mipmap.smile_emoji)
        var allButton_var = arrayOf(
            b00, b01, b02, b03, b04, b05, b06, b07, b08,
            b10, b11, b12, b13, b14, b15, b16, b17, b18,
            b20, b21, b22, b23, b24, b25, b26, b27, b28,
            b30, b31, b32, b33, b34, b35, b36, b37, b38,
            b40, b41, b42, b43, b44, b45, b46, b47, b48,
            b50, b51, b52, b53, b54, b55, b56, b57, b58,
            b60, b61, b62, b63, b64, b65, b66, b67, b68,
            b70, b71, b72, b73, b74, b75, b76, b77, b78,
            b80, b81, b82, b83, b84, b85, b86, b87, b88
        )
        val b00Neighbours = arrayOf(b01, b11, b10)
        val b01Neighbours = arrayOf(b02, b12, b11, b10, b00)
        val b02Neighbours = arrayOf(b03, b13, b12, b11, b01)
        val b03Neighbours = arrayOf(b04, b14, b13, b12, b02)
        val b04Neighbours = arrayOf(b05, b15, b14, b13, b03)
        val b05Neighbours = arrayOf(b06, b16, b15, b14, b04)
        val b06Neighbours = arrayOf(b07, b17, b16, b15, b05)
        val b07Neighbours = arrayOf(b08, b18, b17, b16, b06)
        val b08Neighbours = arrayOf(b18, b17, b07)
        val b10Neighbours = arrayOf(b00, b01, b11, b21, b20)
        val b11Neighbours = arrayOf(b00, b01, b02, b12, b22, b21, b20, b10)
        val b12Neighbours = arrayOf(b01, b02, b03, b13, b23, b22, b21, b11)
        val b13Neighbours = arrayOf(b02, b03, b04, b14, b24, b23, b22, b12)
        val b14Neighbours = arrayOf(b03, b04, b05, b15, b25, b24, b23, b13)
        val b15Neighbours = arrayOf(b04, b05, b06, b16, b26, b25, b24, b14)
        val b16Neighbours = arrayOf(b05, b06, b07, b17, b27, b26, b25, b15)
        val b17Neighbours = arrayOf(b06, b07, b08, b18, b28, b27, b26, b16)
        val b18Neighbours = arrayOf(b07, b08, b28, b27, b17)
        val b20Neighbours = arrayOf(b10, b11, b21, b31, b30)
        val b21Neighbours = arrayOf(b10, b11, b12, b22, b32, b31, b30, b20)
        val b22Neighbours = arrayOf(b11, b12, b13, b23, b33, b32, b31, b21)
        val b23Neighbours = arrayOf(b12, b13, b14, b24, b34, b33, b32, b22)
        val b24Neighbours = arrayOf(b13, b14, b15, b25, b35, b34, b33, b23)
        val b25Neighbours = arrayOf(b14, b15, b16, b26, b36, b35, b34, b24)
        val b26Neighbours = arrayOf(b15, b16, b17, b27, b37, b36, b35, b25)
        val b27Neighbours = arrayOf(b16, b17, b18, b28, b38, b37, b36, b26)
        val b28Neighbours = arrayOf(b17, b18, b38, b37, b27)
        val b30Neighbours = arrayOf(b20, b21, b31, b41, b40)
        val b31Neighbours = arrayOf(b20, b21, b22, b32, b42, b41, b40, b30)
        val b32Neighbours = arrayOf(b21, b22, b23, b33, b43, b42, b41, b31)
        val b33Neighbours = arrayOf(b22, b23, b24, b34, b44, b43, b42, b32)
        val b34Neighbours = arrayOf(b23, b24, b25, b35, b45, b44, b43, b33)
        val b35Neighbours = arrayOf(b24, b25, b26, b36, b46, b45, b44, b34)
        val b36Neighbours = arrayOf(b25, b26, b27, b37, b47, b46, b45, b35)
        val b37Neighbours = arrayOf(b26, b27, b28, b38, b48, b47, b46, b36)
        val b38Neighbours = arrayOf(b27, b28, b48, b47, b37)
        val b40Neighbours = arrayOf(b30, b31, b41, b51, b50)
        val b41Neighbours = arrayOf(b30, b31, b32, b42, b52, b51, b50, b40)
        val b42Neighbours = arrayOf(b31, b32, b33, b43, b53, b52, b51, b41)
        val b43Neighbours = arrayOf(b32, b33, b34, b44, b54, b53, b52, b42)
        val b44Neighbours = arrayOf(b33, b34, b35, b45, b55, b54, b53, b43)
        val b45Neighbours = arrayOf(b34, b35, b36, b46, b56, b55, b54, b44)
        val b46Neighbours = arrayOf(b35, b36, b37, b47, b57, b56, b55, b45)
        val b47Neighbours = arrayOf(b36, b37, b38, b48, b58, b57, b56, b46)
        val b48Neighbours = arrayOf(b37, b38, b58, b57, b47)
        val b50Neighbours = arrayOf(b40, b41, b51, b61, b60)
        val b51Neighbours = arrayOf(b40, b41, b42, b52, b62, b61, b60, b50)
        val b52Neighbours = arrayOf(b41, b42, b43, b53, b63, b62, b61, b51)
        val b53Neighbours = arrayOf(b42, b43, b44, b54, b64, b63, b62, b52)
        val b54Neighbours = arrayOf(b43, b44, b45, b55, b65, b64, b63, b53)
        val b55Neighbours = arrayOf(b44, b45, b46, b56, b66, b65, b64, b54)
        val b56Neighbours = arrayOf(b45, b46, b47, b57, b67, b66, b65, b55)
        val b57Neighbours = arrayOf(b46, b47, b48, b58, b68, b67, b66, b56)
        val b58Neighbours = arrayOf(b47, b48, b68, b67, b57)
        val b60Neighbours = arrayOf(b50, b51, b61, b71, b70)
        val b61Neighbours = arrayOf(b50, b51, b52, b62, b72, b71, b70, b60)
        val b62Neighbours = arrayOf(b51, b52, b53, b63, b73, b72, b71, b61)
        val b63Neighbours = arrayOf(b52, b53, b54, b64, b74, b73, b72, b62)
        val b64Neighbours = arrayOf(b53, b54, b55, b65, b75, b74, b73, b63)
        val b65Neighbours = arrayOf(b54, b55, b56, b66, b76, b75, b74, b64)
        val b66Neighbours = arrayOf(b55, b56, b57, b67, b77, b76, b75, b65)
        val b67Neighbours = arrayOf(b56, b57, b58, b68, b78, b77, b76, b66)
        val b68Neighbours = arrayOf(b57, b58, b78, b77, b67)
        val b70Neighbours = arrayOf(b60, b61, b71, b81, b80)
        val b71Neighbours = arrayOf(b60, b61, b62, b72, b82, b81, b80, b70)
        val b72Neighbours = arrayOf(b61, b62, b63, b73, b83, b82, b81, b71)
        val b73Neighbours = arrayOf(b62, b63, b64, b74, b84, b83, b82, b72)
        val b74Neighbours = arrayOf(b63, b64, b65, b75, b85, b84, b83, b73)
        val b75Neighbours = arrayOf(b64, b65, b66, b76, b86, b85, b84, b74)
        val b76Neighbours = arrayOf(b65, b66, b67, b77, b87, b86, b85, b75)
        val b77Neighbours = arrayOf(b66, b67, b68, b78, b88, b87, b86, b76)
        val b78Neighbours = arrayOf(b67, b68, b88, b87, b77)
        val b80Neighbours = arrayOf(b70, b71, b81)
        val b81Neighbours = arrayOf(b70, b71, b72, b82, b80)
        val b82Neighbours = arrayOf(b71, b72, b73, b83, b81)
        val b83Neighbours = arrayOf(b72, b73, b74, b84, b82)
        val b84Neighbours = arrayOf(b73, b74, b75, b85, b83)
        val b85Neighbours = arrayOf(b74, b75, b76, b86, b84)
        val b86Neighbours = arrayOf(b75, b76, b77, b87, b85)
        val b87Neighbours = arrayOf(b76, b77, b78, b88, b86)
        val b88Neighbours = arrayOf(b77, b78, b87)
        var allButtons = arrayOf(
            b00Neighbours,
            b01Neighbours,
            b02Neighbours,
            b03Neighbours,
            b04Neighbours,
            b05Neighbours,
            b06Neighbours,
            b07Neighbours,
            b08Neighbours,
            b10Neighbours,
            b11Neighbours,
            b12Neighbours,
            b13Neighbours,
            b14Neighbours,
            b15Neighbours,
            b16Neighbours,
            b17Neighbours,
            b18Neighbours,
            b20Neighbours,
            b21Neighbours,
            b22Neighbours,
            b23Neighbours,
            b24Neighbours,
            b25Neighbours,
            b26Neighbours,
            b27Neighbours,
            b28Neighbours,
            b30Neighbours,
            b31Neighbours,
            b32Neighbours,
            b33Neighbours,
            b34Neighbours,
            b35Neighbours,
            b36Neighbours,
            b37Neighbours,
            b38Neighbours,
            b40Neighbours,
            b41Neighbours,
            b42Neighbours,
            b43Neighbours,
            b44Neighbours,
            b45Neighbours,
            b46Neighbours,
            b47Neighbours,
            b48Neighbours,
            b50Neighbours,
            b51Neighbours,
            b52Neighbours,
            b53Neighbours,
            b54Neighbours,
            b55Neighbours,
            b56Neighbours,
            b57Neighbours,
            b58Neighbours,
            b60Neighbours,
            b61Neighbours,
            b62Neighbours,
            b63Neighbours,
            b64Neighbours,
            b65Neighbours,
            b66Neighbours,
            b67Neighbours,
            b68Neighbours,
            b70Neighbours,
            b71Neighbours,
            b72Neighbours,
            b73Neighbours,
            b74Neighbours,
            b75Neighbours,
            b76Neighbours,
            b77Neighbours,
            b78Neighbours,
            b80Neighbours,
            b81Neighbours,
            b82Neighbours,
            b83Neighbours,
            b84Neighbours,
            b85Neighbours,
            b86Neighbours,
            b87Neighbours,
            b88Neighbours
        )

        var b00MineCount = 0
        var b01MineCount = 0
        var b02MineCount = 0
        var b03MineCount = 0
        var b04MineCount = 0
        var b05MineCount = 0
        var b06MineCount = 0
        var b07MineCount = 0
        var b08MineCount = 0
        var b10MineCount = 0
        var b11MineCount = 0
        var b12MineCount = 0
        var b13MineCount = 0
        var b14MineCount = 0
        var b15MineCount = 0
        var b16MineCount = 0
        var b17MineCount = 0
        var b18MineCount = 0
        var b20MineCount = 0
        var b21MineCount = 0
        var b22MineCount = 0
        var b23MineCount = 0
        var b24MineCount = 0
        var b25MineCount = 0
        var b26MineCount = 0
        var b27MineCount = 0
        var b28MineCount = 0
        var b30MineCount = 0
        var b31MineCount = 0
        var b32MineCount = 0
        var b33MineCount = 0
        var b34MineCount = 0
        var b35MineCount = 0
        var b36MineCount = 0
        var b37MineCount = 0
        var b38MineCount = 0
        var b40MineCount = 0
        var b41MineCount = 0
        var b42MineCount = 0
        var b43MineCount = 0
        var b44MineCount = 0
        var b45MineCount = 0
        var b46MineCount = 0
        var b47MineCount = 0
        var b48MineCount = 0
        var b50MineCount = 0
        var b51MineCount = 0
        var b52MineCount = 0
        var b53MineCount = 0
        var b54MineCount = 0
        var b55MineCount = 0
        var b56MineCount = 0
        var b57MineCount = 0
        var b58MineCount = 0
        var b60MineCount = 0
        var b61MineCount = 0
        var b62MineCount = 0
        var b63MineCount = 0
        var b64MineCount = 0
        var b65MineCount = 0
        var b66MineCount = 0
        var b67MineCount = 0
        var b68MineCount = 0
        var b70MineCount = 0
        var b71MineCount = 0
        var b72MineCount = 0
        var b73MineCount = 0
        var b74MineCount = 0
        var b75MineCount = 0
        var b76MineCount = 0
        var b77MineCount = 0
        var b78MineCount = 0
        var b80MineCount = 0
        var b81MineCount = 0
        var b82MineCount = 0
        var b83MineCount = 0
        var b84MineCount = 0
        var b85MineCount = 0
        var b86MineCount = 0
        var b87MineCount = 0
        var b88MineCount = 0
        var allButtonMineCounts = arrayOf(
            b00MineCount,
            b01MineCount,
            b02MineCount,
            b03MineCount,
            b04MineCount,
            b05MineCount,
            b06MineCount,
            b07MineCount,
            b08MineCount,
            b10MineCount,
            b11MineCount,
            b12MineCount,
            b13MineCount,
            b14MineCount,
            b15MineCount,
            b16MineCount,
            b17MineCount,
            b18MineCount,
            b20MineCount,
            b21MineCount,
            b22MineCount,
            b23MineCount,
            b24MineCount,
            b25MineCount,
            b26MineCount,
            b27MineCount,
            b28MineCount,
            b30MineCount,
            b31MineCount,
            b32MineCount,
            b33MineCount,
            b34MineCount,
            b35MineCount,
            b36MineCount,
            b37MineCount,
            b38MineCount,
            b40MineCount,
            b41MineCount,
            b42MineCount,
            b43MineCount,
            b44MineCount,
            b45MineCount,
            b46MineCount,
            b47MineCount,
            b48MineCount,
            b50MineCount,
            b51MineCount,
            b52MineCount,
            b53MineCount,
            b54MineCount,
            b55MineCount,
            b56MineCount,
            b57MineCount,
            b58MineCount,
            b60MineCount,
            b61MineCount,
            b62MineCount,
            b63MineCount,
            b64MineCount,
            b65MineCount,
            b66MineCount,
            b67MineCount,
            b68MineCount,
            b70MineCount,
            b71MineCount,
            b72MineCount,
            b73MineCount,
            b74MineCount,
            b75MineCount,
            b76MineCount,
            b77MineCount,
            b78MineCount,
            b80MineCount,
            b81MineCount,
            b82MineCount,
            b83MineCount,
            b84MineCount,
            b85MineCount,
            b86MineCount,
            b87MineCount,
            b88MineCount

        )

        var b00isMine = false
        var b01isMine = false
        var b02isMine = false
        var b03isMine = false
        var b04isMine = false
        var b05isMine = false
        var b06isMine = false
        var b07isMine = false
        var b08isMine = false
        var b10isMine = false
        var b11isMine = false
        var b12isMine = false
        var b13isMine = false
        var b14isMine = false
        var b15isMine = false
        var b16isMine = false
        var b17isMine = false
        var b18isMine = false
        var b20isMine = false
        var b21isMine = false
        var b22isMine = false
        var b23isMine = false
        var b24isMine = false
        var b25isMine = false
        var b26isMine = false
        var b27isMine = false
        var b28isMine = false
        var b30isMine = false
        var b31isMine = false
        var b32isMine = false
        var b33isMine = false
        var b34isMine = false
        var b35isMine = false
        var b36isMine = false
        var b37isMine = false
        var b38isMine = false
        var b40isMine = false
        var b41isMine = false
        var b42isMine = false
        var b43isMine = false
        var b44isMine = false
        var b45isMine = false
        var b46isMine = false
        var b47isMine = false
        var b48isMine = false
        var b50isMine = false
        var b51isMine = false
        var b52isMine = false
        var b53isMine = false
        var b54isMine = false
        var b55isMine = false
        var b56isMine = false
        var b57isMine = false
        var b58isMine = false
        var b60isMine = false
        var b61isMine = false
        var b62isMine = false
        var b63isMine = false
        var b64isMine = false
        var b65isMine = false
        var b66isMine = false
        var b67isMine = false
        var b68isMine = false
        var b70isMine = false
        var b71isMine = false
        var b72isMine = false
        var b73isMine = false
        var b74isMine = false
        var b75isMine = false
        var b76isMine = false
        var b77isMine = false
        var b78isMine = false
        var b80isMine = false
        var b81isMine = false
        var b82isMine = false
        var b83isMine = false
        var b84isMine = false
        var b85isMine = false
        var b86isMine = false
        var b87isMine = false
        var b88isMine = false
        var allButtonIsMine = arrayOf(
            b00isMine,
            b01isMine,
            b02isMine,
            b03isMine,
            b04isMine,
            b05isMine,
            b06isMine,
            b07isMine,
            b08isMine,
            b10isMine,
            b11isMine,
            b12isMine,
            b13isMine,
            b14isMine,
            b15isMine,
            b16isMine,
            b17isMine,
            b18isMine,
            b20isMine,
            b21isMine,
            b22isMine,
            b23isMine,
            b24isMine,
            b25isMine,
            b26isMine,
            b27isMine,
            b28isMine,
            b30isMine,
            b31isMine,
            b32isMine,
            b33isMine,
            b34isMine,
            b35isMine,
            b36isMine,
            b37isMine,
            b38isMine,
            b40isMine,
            b41isMine,
            b42isMine,
            b43isMine,
            b44isMine,
            b45isMine,
            b46isMine,
            b47isMine,
            b48isMine,
            b50isMine,
            b51isMine,
            b52isMine,
            b53isMine,
            b54isMine,
            b55isMine,
            b56isMine,
            b57isMine,
            b58isMine,
            b60isMine,
            b61isMine,
            b62isMine,
            b63isMine,
            b64isMine,
            b65isMine,
            b66isMine,
            b67isMine,
            b68isMine,
            b70isMine,
            b71isMine,
            b72isMine,
            b73isMine,
            b74isMine,
            b75isMine,
            b76isMine,
            b77isMine,
            b78isMine,
            b80isMine,
            b81isMine,
            b82isMine,
            b83isMine,
            b84isMine,
            b85isMine,
            b86isMine,
            b87isMine,
            b88isMine
        )
        //restarts game deletes all mines
        for (i in allButton_var.size - 1 downTo 0) {
            allButton_var[i].isClickable = true
            allButton_var[i].isLongClickable = true
            allButton_var[i].setBackgroundResource(R.mipmap.cell)
            allButton_var[i].text = ""
        }
        //shuffles array


        //makes button mine  background image mine
        var countToTen1 = 0
        var countToTen2 = 0
        while (countToTen2 < minesCount) {
            val j = Random.nextInt(allButton_var.size - 1)
            allButton_var[j].text = "M"
            for (i in allButton_var) {
                if (i.text.contains("M")) {
                    countToTen1 += 1

                }
            }
            countToTen2 = countToTen1
            countToTen1 = 0
        }


//       counts mines around cells
        for (i in allButton_var.size - 1 downTo 0) {
            if (allButton_var[i].text != "M")
                for (j in allButtons[i]) {
                    if (j.text.equals("M")) {
                        allButtonMineCounts[i] += 1
                        allButton_var[i].text = allButtonMineCounts[i].toString()
                    }
                }

        }

//        for (i in allButton_var.size - 1 downTo 0) {
//            if (allButtonMineCounts[i] == 1 && !allButtonIsMine[i]) {
//                a
//                continue
//            } else if (allButtonMineCounts[i] == 2 && !allButtonIsMine[i]) {
//                allButton_var[i].setBackgroundResource(R.mipmap.cell_2)
//                continue
//            } else if (allButtonMineCounts[i] == 3 && !allButtonIsMine[i]) {
//                allButton_var[i].setBackgroundResource(R.mipmap.cell_3)
//                continue
//            } else if (allButtonMineCounts[i] == 4 && !allButtonIsMine[i]) {
//                allButton_var[i].setBackgroundResource(R.mipmap.cell_4)
//                continue
//            } else if (allButtonMineCounts[i] == 5 && !allButtonIsMine[i]) {
//                allButton_var[i].setBackgroundResource(R.mipmap.cell_5)
//                continue
//            } else if (allButtonMineCounts[i] == 6 && !allButtonIsMine[i]) {
//                allButton_var[i].setBackgroundResource(R.mipmap.cell_6)
//                continue
//            } else if (allButtonMineCounts[i] == 7 && !allButtonIsMine[i]) {
//                allButton_var[i].setBackgroundResource(R.mipmap.cell_7)
//                continue
//            } else if (allButtonMineCounts[i] == 8 && !allButtonIsMine[i]) {
//                allButton_var[i].setBackgroundResource(R.mipmap.cell_8)
//                continue
//            } else if (allButtonMineCounts[i] == 0 && !allButtonIsMine[i]) {
//                allButton_var[i].setBackgroundResource(R.mipmap.cell_0)
//            }
//
//        }

    }

    private fun openCell(button: Button) {
        button.setOnClickListener {
            gameIsActive = true
            var allButton_var = arrayOf(
                b00, b01, b02, b03, b04, b05, b06, b07, b08,
                b10, b11, b12, b13, b14, b15, b16, b17, b18,
                b20, b21, b22, b23, b24, b25, b26, b27, b28,
                b30, b31, b32, b33, b34, b35, b36, b37, b38,
                b40, b41, b42, b43, b44, b45, b46, b47, b48,
                b50, b51, b52, b53, b54, b55, b56, b57, b58,
                b60, b61, b62, b63, b64, b65, b66, b67, b68,
                b70, b71, b72, b73, b74, b75, b76, b77, b78,
                b80, b81, b82, b83, b84, b85, b86, b87, b88
            )

            if (button.text == "1" && !button.text.contains("F")) {
                button.setBackgroundResource(R.mipmap.cell_1)
            } else if (button.text == "2" && !button.text.contains("F")) {
                button.setBackgroundResource(R.mipmap.cell_2)
            } else if (button.text == "3" && !button.text.contains("F")) {
                button.setBackgroundResource(R.mipmap.cell_3)
            } else if (button.text == "4" && !button.text.contains("F")) {
                button.setBackgroundResource(R.mipmap.cell_4)
            } else if (button.text == "5" && !button.text.contains("F")) {
                button.setBackgroundResource(R.mipmap.cell_5)
            } else if (button.text == "6" && !button.text.contains("F")) {
                button.setBackgroundResource(R.mipmap.cell_6)
            } else if (button.text == "7" && !button.text.contains("F")) {
                button.setBackgroundResource(R.mipmap.cell_7)
            } else if (button.text == "8" && !button.text.contains("F")) {
                button.setBackgroundResource(R.mipmap.cell_8)
            } else if (button.text == "" && !button.text.contains("F")) {
                button.setBackgroundResource(R.mipmap.cell_0)
            } else if (button.text == "M" && !button.text.contains("F")) {
                gameIsActive = false
                newGameButton.isClickable = false
                Handler().postDelayed({
                    button.setBackgroundResource(R.mipmap.mine)
                }, (500))
                for (i in allButton_var) {
                    i.isClickable = false
                    i.isLongClickable = false
                }
                for (i in allButton_var) {
                    if (i.text == "M") {
                        Handler().postDelayed({
                            i.setBackgroundResource(R.mipmap.mine)
                        }, Random.nextInt(1000, 2500).toLong())
                    }
                }
                newGameButton.setBackgroundResource(R.mipmap.injured_emoji)
                newGameButton.isClickable = true
            }
            var minesWithFlagCount = 0
            for (i in allButton_var) {
                if (i.text.contains("M") && i.text.contains("F")) {
                    minesWithFlagCount += 1
                }
                if (minesWithFlagCount == minesCount) {
                    newGameButton.setBackgroundResource(R.mipmap.sunglasses_emoji)
                    gameIsActive = false
                    for (i in allButton_var) {
                        i.isClickable = false
                        i.isLongClickable = false
                    }
                    var winText = "You Won With: " + currentPointsView.text.toString() + "points!"
                    Toast.makeText(this, winText, Toast.LENGTH_SHORT).show()
                    break
                }
            }

        }

    }


    //    fun main() {
//        val scores = arrayOf(Score("Alex", 5), Score("Bob", 2), Score("Harry", 1))
//
//        println("Original array : ")
//        scores.forEach { println(it) }
//
//        scores.sortBy { student -> student.points}
//
//        println("Final array : ")
//        scores.forEach { println(it) }
//    }
    private fun setFlag(button: Button) {
        var allButton_var = arrayOf(
            b00, b01, b02, b03, b04, b05, b06, b07, b08,
            b10, b11, b12, b13, b14, b15, b16, b17, b18,
            b20, b21, b22, b23, b24, b25, b26, b27, b28,
            b30, b31, b32, b33, b34, b35, b36, b37, b38,
            b40, b41, b42, b43, b44, b45, b46, b47, b48,
            b50, b51, b52, b53, b54, b55, b56, b57, b58,
            b60, b61, b62, b63, b64, b65, b66, b67, b68,
            b70, b71, b72, b73, b74, b75, b76, b77, b78,
            b80, b81, b82, b83, b84, b85, b86, b87, b88
        )
        button.setOnLongClickListener(object : View.OnLongClickListener {
            override fun onLongClick(p0: View?): Boolean {
                if (!button.text.contains("F")) {
                    button.setBackgroundResource(R.mipmap.flag)
                    button.text = button.text.toString() + "F"
                } else if (button.text.contains("F")) {
                    button.setBackgroundResource(R.mipmap.cell)
                    button.text = button.text.toString().replace("F", "")
                }
                var minesWithFlagCount = 0
                for (i in allButton_var) {
                    if (i.text.contains("M") && i.text.contains("F")) {
                        minesWithFlagCount += 1
                    }
                    if (minesWithFlagCount == 10) {
                        gameIsActive = false
                        newGameButton.setBackgroundResource(R.mipmap.sunglasses_emoji)
                        for (i in allButton_var) {
                            i.isClickable = false
                        }
                        var winText =
                            "You Won With: " + currentPointsView.text.toString() + "points!"
                        Toast.makeText(this@GameActivity, winText, Toast.LENGTH_LONG).show()
                        break
                    }
                }
                return true
            }

        })
    }


    private fun accountPage() {
        accountButton_game.setOnClickListener {
            val intent = Intent(this, LogInActivity::class.java)
            startActivity(intent)
        }
    }

    private fun guidePage() {
        guideButton_game.setOnClickListener {
            val intent = Intent(this, GuideActivity::class.java)
            startActivity(intent)
        }
    }

    private fun leaderboardPage() {
        leaderboardButton_game.setOnClickListener {
            val intent = Intent(this, LeaderboardActivity::class.java)
            startActivity(intent)
        }
    }

    var gameIsActive: Boolean = false
    var currentTime = 0
    var minesCount = 10
    private fun countTime() {
        Handler().postDelayed({
            setTime()
        }, 1000)
    }

    private fun setTime() {
        if (gameIsActive) {
            currentTime += 1
        }
        if (currentTime != 0 ) {
            currentPointsView.text = ((10000 * minesCount) - currentTime * 500).toString()
        }
        if (currentTime > 999 ) {
            currentTimeView.text = 999.toString()
        } else if (currentTime > 9 && currentTime < 100 ) {
            currentTimeView.text = ("0" + currentTime.toString())
        } else if (currentTime > 99 && currentTime < 1000 ) {
            currentTimeView.text = currentTime.toString()
        } else {
            currentTimeView.text = ("00" + currentTime.toString())
        }
        countTime()
    }



    private fun setDefaultImagesToButtons() {
        b00.setBackgroundResource(R.mipmap.cell)
        b01.setBackgroundResource(R.mipmap.cell)
        b02.setBackgroundResource(R.mipmap.cell)
        b03.setBackgroundResource(R.mipmap.cell)
        b04.setBackgroundResource(R.mipmap.cell)
        b05.setBackgroundResource(R.mipmap.cell)
        b06.setBackgroundResource(R.mipmap.cell)
        b07.setBackgroundResource(R.mipmap.cell)
        b08.setBackgroundResource(R.mipmap.cell)
        b10.setBackgroundResource(R.mipmap.cell)
        b11.setBackgroundResource(R.mipmap.cell)
        b12.setBackgroundResource(R.mipmap.cell)
        b13.setBackgroundResource(R.mipmap.cell)
        b14.setBackgroundResource(R.mipmap.cell)
        b15.setBackgroundResource(R.mipmap.cell)
        b16.setBackgroundResource(R.mipmap.cell)
        b17.setBackgroundResource(R.mipmap.cell)
        b18.setBackgroundResource(R.mipmap.cell)
        b20.setBackgroundResource(R.mipmap.cell)
        b21.setBackgroundResource(R.mipmap.cell)
        b22.setBackgroundResource(R.mipmap.cell)
        b23.setBackgroundResource(R.mipmap.cell)
        b24.setBackgroundResource(R.mipmap.cell)
        b25.setBackgroundResource(R.mipmap.cell)
        b26.setBackgroundResource(R.mipmap.cell)
        b27.setBackgroundResource(R.mipmap.cell)
        b28.setBackgroundResource(R.mipmap.cell)
        b30.setBackgroundResource(R.mipmap.cell)
        b31.setBackgroundResource(R.mipmap.cell)
        b32.setBackgroundResource(R.mipmap.cell)
        b33.setBackgroundResource(R.mipmap.cell)
        b34.setBackgroundResource(R.mipmap.cell)
        b35.setBackgroundResource(R.mipmap.cell)
        b36.setBackgroundResource(R.mipmap.cell)
        b37.setBackgroundResource(R.mipmap.cell)
        b38.setBackgroundResource(R.mipmap.cell)
        b40.setBackgroundResource(R.mipmap.cell)
        b41.setBackgroundResource(R.mipmap.cell)
        b42.setBackgroundResource(R.mipmap.cell)
        b43.setBackgroundResource(R.mipmap.cell)
        b44.setBackgroundResource(R.mipmap.cell)
        b45.setBackgroundResource(R.mipmap.cell)
        b46.setBackgroundResource(R.mipmap.cell)
        b47.setBackgroundResource(R.mipmap.cell)
        b48.setBackgroundResource(R.mipmap.cell)
        b50.setBackgroundResource(R.mipmap.cell)
        b51.setBackgroundResource(R.mipmap.cell)
        b52.setBackgroundResource(R.mipmap.cell)
        b53.setBackgroundResource(R.mipmap.cell)
        b54.setBackgroundResource(R.mipmap.cell)
        b55.setBackgroundResource(R.mipmap.cell)
        b56.setBackgroundResource(R.mipmap.cell)
        b57.setBackgroundResource(R.mipmap.cell)
        b58.setBackgroundResource(R.mipmap.cell)
        b60.setBackgroundResource(R.mipmap.cell)
        b61.setBackgroundResource(R.mipmap.cell)
        b62.setBackgroundResource(R.mipmap.cell)
        b63.setBackgroundResource(R.mipmap.cell)
        b64.setBackgroundResource(R.mipmap.cell)
        b65.setBackgroundResource(R.mipmap.cell)
        b66.setBackgroundResource(R.mipmap.cell)
        b67.setBackgroundResource(R.mipmap.cell)
        b68.setBackgroundResource(R.mipmap.cell)
        b70.setBackgroundResource(R.mipmap.cell)
        b71.setBackgroundResource(R.mipmap.cell)
        b72.setBackgroundResource(R.mipmap.cell)
        b73.setBackgroundResource(R.mipmap.cell)
        b74.setBackgroundResource(R.mipmap.cell)
        b75.setBackgroundResource(R.mipmap.cell)
        b76.setBackgroundResource(R.mipmap.cell)
        b77.setBackgroundResource(R.mipmap.cell)
        b78.setBackgroundResource(R.mipmap.cell)
        b80.setBackgroundResource(R.mipmap.cell)
        b81.setBackgroundResource(R.mipmap.cell)
        b82.setBackgroundResource(R.mipmap.cell)
        b83.setBackgroundResource(R.mipmap.cell)
        b84.setBackgroundResource(R.mipmap.cell)
        b85.setBackgroundResource(R.mipmap.cell)
        b86.setBackgroundResource(R.mipmap.cell)
        b87.setBackgroundResource(R.mipmap.cell)
        b88.setBackgroundResource(R.mipmap.cell)
    }

}

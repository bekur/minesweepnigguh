package com.example.minesweeper
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_leaderboard.*
import kotlinx.android.synthetic.main.activity_log_in.*
class LogInActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)
        auth = FirebaseAuth.getInstance()
        back()
        sign()
        logIn()
        logOut()
        val pref = PreferenceManager.getDefaultSharedPreferences(this)
        pref.apply {
            val currentUser = getString("CurrentUser", "")
            if (currentUser != ""){
                currentUserView.setText(currentUser)
            }
        }

    }

    private fun logOut(){
        logOutButton.setOnClickListener {
            val pref = PreferenceManager.getDefaultSharedPreferences(this)
            val editor = pref.edit()
            editor
                .putString("CurrentUser", "user.name@example.com")
                .apply()
            currentUserView.text = "user.name@example.com"
            Toast.makeText(
                baseContext, "თქვენ გამოსული ხართ ანგარიშიდან",
                Toast.LENGTH_SHORT
            ).show()
        }
    }
    private fun saveData(){
        val pref = PreferenceManager.getDefaultSharedPreferences(this)
        val editor = pref.edit()

        editor
            .putString("CurrentUser", emailField.text.toString())
            .apply()
    }

    private fun back() {
        backButton_logIn.setOnClickListener {
            val intent = Intent(this, GameActivity::class.java)
            startActivity(intent)
        }
    }
    private fun sign() {
        SignUpButton_logIn_.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }
    }

    private fun logIn() {
        logInButton_logIn.setOnClickListener {
            checkFields()
        }
    }
    private fun checkFields() {
        if (emailField.text.isEmpty() && passwordField.text.isNotEmpty()) {
            Toast.makeText(this, "გთხოვთ, შეიყვანოთ თქვენი იმეილი", Toast.LENGTH_SHORT).show()
        } else if (emailField.text.isNotEmpty() && passwordField.text.isEmpty()) {
            Toast.makeText(this, "გთხოვთ, შეიყვანოთ თქვენი პაროლი", Toast.LENGTH_SHORT).show()
        } else if (emailField.text.isEmpty() && passwordField.text.isEmpty()) {
            Toast.makeText(this, "გთხოვთ, შეავსოთ ყველა ველი", Toast.LENGTH_SHORT).show()
        } else {
            logInCheck()
        }
    }
    private fun logInCheck() {
        auth.signInWithEmailAndPassword(emailField.text.toString(), passwordField.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Log.d("signIn", "signInWithEmail:success")
                    intent.putExtra("email",emailField.text.toString())
                    Toast.makeText(
                        baseContext, "ავტორიზაცია წარატებით დასრულდა",
                        Toast.LENGTH_SHORT
                    ).show()
                    saveData()
                    currentUserView.text = emailField.text.toString()
                } else {
                    Log.w("signIn", "signInWithEmail:failure", task.exception)
                    Toast.makeText(
                        baseContext, "ავტორიზაცია ვერ მოხერხდა",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
    }
}

